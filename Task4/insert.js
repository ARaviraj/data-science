const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'globaltemperature';

MongoClient.connect(connectionURL, { useNewUrlParser: true, useUnifiedTopology: true }, async (error, client) => {
    if (error) {
        return console.log('Unable to connect to database!')
    }
    const database = client.db(databaseName);
    if (database) {
        console.log("Database connected successfully");
        const temperature = database.collection("temperature");

        const temperdata = {
            month_date:'2020-03',
            anomaly_value:'-0.306',
            upper_95_ci:'-0.368',
            lower_95_ci:' -0.253'
        };
          temperature.insertOne(temperdata).then((result)=>{
            console.log("insertId",result.insertedId);
            client.close();
            }).catch((error)=>{
            console.log(error);
            });
    }



})
