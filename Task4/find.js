
import { MongoClient as _MongoClient } from 'mongodb';
const MongoClient = _MongoClient
const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'globaltemperature';
MongoClient.connect(connectionURL, { useNewUrlParser: true, useUnifiedTopology: true }, async (error, client) => {
    if (error) {
        return console.log('Unable to connect to database!')
    }
    const database = client.db(databaseName);
    if (database) {
        console.log("Database connected successfully");
    database.collection('temperature').find({  }).toArray((error, temperature) => {
         console.log(temperature)
    })
    };

})