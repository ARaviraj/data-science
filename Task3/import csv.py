import csv
import pymongo


myclient = pymongo.MongoClient("mongodb://localhost:27017/");
mydb = myclient["globaltemperature"]
mycol = mydb["temperature"]


with open('Global Temperature Anomalies.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')

    thislist = []

    for row in readCSV:
        thisdict = {
            "month_date": row[0],
            "anomaly_value": row[1],
            "upper_95_ci": row[2],
            "lower_95_ci": row[3]
        }

        thislist.append(thisdict)
        mycol.insert_one(thisdict)

